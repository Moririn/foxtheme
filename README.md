# Firefox theme installer
Automated installation of userChrome theming for Firefox and its derivatives' interface via a shell script.
Included theme originally modified from [Filip Sutkowy's *blurclean-firefox-theme* (GitHub)](https://github.com/Filip-Sutkowy/blurclean-firefox-theme).

## Description
This script will create or modify the following files and directories:
+ `$XDG_CONFIG_HOME/foxtheme/chrome/` and subsidiary CSS files will be created if they do not exist, or overwritten if `-o` is passed, in order to provide the necessary styles
+ `<browser profile directory>/chrome` will be created as a symlink to `$XDG_CONFIG_HOME/foxtheme/chrome/`
+ `<browser profile directory>/user.js` will be created if it does not exist, or appended to in the case of an existing `user.js`, in order to modify the necessary `about:config` settings for the theme
+ `<browser profile directory>/user-overrides.js` will be created instead of `user.js` if it does not exist and `-r` is passed, or appended to in the case of an existing `user-overrides.js`, with the same modifications as the regular `user.js` file in order to ensure current or a potential future compatibility with the [arkenfox user.js (GitHub)](https://github.com/arkenfox/user.js) or similar

## Options
```
Usage: ./foxtheme.sh [OPTIONS] [PROFILE | -a PROFILESDIR]
-h		Display this help message
-o		Remove and replace an existing theme if found
-a		Apply theme to all (current) browser profiles
-r		Apply modifications to user-overrides.js instead of user.js
-d		Don't modify existing user.js values. May cause the theme to behave incorrectly
```
## Custom themes
This script is ultimately just a glorified wrapper for:
```
mkdir $XDG_CONFIG_DIR/foxtheme/
cp -r chrome/ $XDG_CONFIG_DIR/foxtheme/
ln -s -f $XDG_CONFIG_DIR/foxtheme/chrome/ <browser profile directory>
cat user.js >> <browser profile directory>/user.js
```
As such, replacing the supplied `user.js` or contents of the `chrome` directory with custom preferences and themes should offer the same functionality.
