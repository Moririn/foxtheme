#!/bin/sh

SCRIPTLOCATION=$(dirname -- "$(readlink -f "$0")")
XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
THEMEDIR="$XDG_CONFIG_HOME/foxtheme"
[ -d "$SCRIPTLOCATION" ] || { echo "$0: Could not verify script location as an existing directory. This is probably a bug."; exit 2; }
[ -d "$SCRIPTLOCATION"/chrome ] || { echo "$0: Could not proceed with installation as 'chrome' directory is missing from '$SCRIPTLOCATION'."; exit 2; }
[ -f "$SCRIPTLOCATION"/user.js ] || { echo "$0: Could not proceed with installation as 'user.js' file is missing from '$SCRIPTLOCATION'."; exit 2; }

helptext() {
	echo "Usage: ./foxtheme.sh [OPTIONS] [PROFILE | -a PROFILESDIR] "
	echo "-h		Display this help message"
	echo "-o		Remove and replace an existing theme if found"
	echo "-a		Apply theme to all (current) browser profiles"
	echo "-r		Apply modifications to user-overrides.js instead of user.js"
	echo "-d		Don't modify existing user.js values. May cause the theme to behave incorrectly."
}

installtoprofile() {
	targetprofile="$1"
	if [ "$OVERRIDES" ]; then
		userjs="${targetprofile}/user-overrides.js"
	else
		userjs="${targetprofile}/user.js"
		[ -f "$userjs" ] && echo "$0: Warning - Existing user.js file found at '$userjs' (pass -r to modify user-overrides.js instead)"
	fi
	[ -d "$targetprofile" ] || { echo "$0: Profile at '$targetprofile' does not exist"; exit 2; }
	[ -d "$targetprofile/chrome/" ] && {
		[ "$OVERWRITE" ] || { echo "$0: A theme for this profile already exists at '${targetprofile}/chrome/'. Pass -o to overwrite existing themes."; exit 2; }
		rm -r -f "${targetprofile:?}/chrome"
	}
	[ -d "$THEMEDIR"/chrome ] && {
		if ! diff -q "${SCRIPTLOCATION}/chrome" "$THEMEDIR/chrome" >/dev/null 2>&1; then
			[ "$OVERWRITE" ] || { echo "$0: A theme already exists at '$THEMEDIR/chrome/'. Pass -o to overwrite existing themes."; exit 2; }
			rm -r -f "${THEMEDIR:?}/chrome"
		fi
	}

	mkdir -p "$THEMEDIR/"
	cp -r "${SCRIPTLOCATION:?}/chrome/" "${THEMEDIR:?}"
	ln -s -f "${THEMEDIR:?}/chrome/" "${targetprofile:?}"
	[ -f "$userjs" ] || touch "$userjs"
	for pref in $(sed --posix -e "s/^.*(\"//" -e "s/\",.*)\;$//" "${SCRIPTLOCATION}/user.js"); do
		if [ "$NOCHANGEJS" ]; then
			grep -q -s "$pref" "$userjs" || grep "\"$pref\"" "${SCRIPTLOCATION}/user.js" >> "$userjs"
		else
			sed -i "/\"$pref\"/d" "${userjs:?}"
			grep "\"$pref\"" "${SCRIPTLOCATION}/user.js" >> "${userjs:?}"
		fi
	done
	echo "Successfully installed to '${targetprofile}'"
}

while getopts "hoard" opt; do
	case $opt in
		a)
			ALL=1
			;;
		d)
			NOCHANGEJS=1
			;;
		o)
			OVERWRITE=1
			;;
		r)
			OVERRIDES=1
			;;
		h)
			helptext
			exit
			;;
		*)
			helptext
			exit 2
			;;
	esac
done

[ -z "$*" ] && { helptext; exit; }
shift $((OPTIND -1))

[ "$2" ] && { echo "$0: Too many arguments"; helptext; exit 2; }

[ -d "$1" ] || { echo "$0: '$1' is not a valid directory"; helptext; exit 2; }

arg="$1"

if [ "$ALL" ]; then
	profilesdir=$(realpath "$arg")
	[ -f "$profilesdir"/installs.ini ] && [ -f "$profilesdir"/profiles.ini ] || { echo "$0: '$profilesdir' does not appear to be a valid Firefox (or derivative) profiles directory (could not find 'profiles.ini' or 'installs.ini'). Run without the -a option if you intended to apply the theme to only a single profile."; exit 2; }
	IFS_=$IFS
	IFS="
	"
	profiles=$(grep "^Path=" "$profilesdir/profiles.ini" | sed "s/^Path=//")
	for profile in $profiles; do
		installtoprofile "$profilesdir/$profile"
	done
	IFS=$IFS_
else
	profile=$(realpath "$arg")
	[ -f "$profile/times.json" ] || { echo "$0: '$profile' does not appear to be a valid Firefox (or derivative) profile directory (could not find 'times.json')."; exit 2; }
	installtoprofile "$profile"
fi

echo "All done."
